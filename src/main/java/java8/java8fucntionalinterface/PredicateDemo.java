package java8.java8fucntionalinterface;

import java.util.Arrays;
import java.util.List;


//refer youtube tutorial java8 Functional interface on JavaTechie youtube channel
public class PredicateDemo {
/*    @Override
    public boolean test(Integer integer) {
        if(integer%2==0)
            return true;
        else
            return false;
    }*/

    public static void main(String[] args) {
       // Predicate<Integer> predicateDemo =  integer-> integer % 2 == 0;
        //System.out.println(predicateDemo.test(2));

        List<Integer> list= Arrays.asList(1,2,5,433,2);
        list.stream().filter(integer-> integer % 2 == 0).forEach(t->System.out.println(t));

    }
}

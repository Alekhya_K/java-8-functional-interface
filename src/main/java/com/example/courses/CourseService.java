package com.example.courses;

import com.example.courses.repositories.CourseRepository;
import com.example.courses.domain.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;
   /*List<Topic> var =new ArrayList<>(Arrays.asList(
            new Topic("1","Alekhya","Good"),
			new Topic("2", "Akshara","bad"),
            new Topic("3","Damu","bad"),
            new Topic("4","jyothi","ok")));*/

   public List<Course> getallcourses(String topicId)
   {
       //return var;
       List<Course> courses = new ArrayList<>();
       courseRepository.findByTopicId(topicId).forEach(courses::add); //refer javabrains tutorial on java8 lambda expressions
       return courses;

   }
   public Course getcourse(String id)
   {
     // return var.stream().filter(t -> t.getId().equals(id)).findFirst().get();

       return courseRepository.findById(id).orElse(null);
   }

    public void addCourse(Course course) {

       //var.add(topic);

        courseRepository.save(course);
    }

    public void deleteCourse(String id) {
/*       Topic topic;
       topic = var.stream().filter(t -> t.getId().equals(id)).findFirst().get();
       var.remove(topic);*/

      courseRepository.deleteById(id);

    }

    public void updateCourse(Course course) {

/*       for(int i=0;i<var.size();i++)
       {
          Topic t= var.get(i);
          if(t.getId().equals(id))
          {
              var.set(i,topic);
              return;
          }
       }*/

       courseRepository.save(course);
    }
}
